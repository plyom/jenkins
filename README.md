# SETUP

1. [install docker](https://docs.docker.com/engine/installation/)
2. [install docker-compose](https://docs.docker.com/compose/install/)

# RUN

1. $ docker-compose build
2. $ docker-compose up -d

# DETAILS

This project will run on Docker Containers: jenkins-master, jenkins-data and jenkins nginx.

### jenkins-master

Container that will run the jenkins application.
This container uses 2 volumes from jenkins-data container: /var/jenkins_home and /var/log/jenkins.
The Dockerfile install docker and docker-compose so it is possible to instantiate Docker containers inside Jenkins Jobs.
It also starts Jenkins with the logfile pointing to the correct folder and moving the .war (excecutable) jenkins file out of /var/jenkins_home to avoid problems with upgrading Jenkins.

**TODO:**

- Provide a good way to use and control Jenkins Slaves with Docker.

### jenkins-data

Container that provides the volumes for jenkins-master.

### jenkins-nginx

Container that exposes the Jenkins application to the port 80.

**TODO:**

- Provide a good way to control DNS management with Docker.
